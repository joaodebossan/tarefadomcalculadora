let notas = [];

function adicionarNota() { // Função do botão Adicionar
    const notaInput = document.getElementById("nota");
    const nota = parseFloat(notaInput.value);// Convertendo o valor para um número float.

    // Verificando se a nota é um número válido (não é NaN).
    if (!isNaN(nota)) {
        notas.push(nota);
        atualizarNotasLista(); // atualizar a lista de notas exibida na tela.
        notaInput.value = ""; // Limpando o campo de input
    }
}

// Função para atualizar a lista de notas exibida na tela.
function atualizarNotasLista() {
    const notasLista = document.getElementById("notasLista");
    notasLista.innerHTML = notas.join(", "); // Atualizando o conteúdo do elemento com a lista de notas formatada como uma string separada por vírgulas.
}

function calcularMedia() { // Função botão "Calcular média".
    const resultadoMedia = document.getElementById("resultadoMedia");

    if (notas.length === 0) { //Verificando se o array de notas está vazio, ou seja, se o usuário não digitou nenhuma nota.
        resultadoMedia.textContent = "Insira pelo menos uma nota."; // mensagem caso nao existam notas
        return; // Retornamos para evitar que o cálculo seja feito com um array vazio.
    }

    // Caso haja notas no array, realizamos o cálculo da média.
    const soma = notas.reduce((acc, nota) => acc + nota, 0); // Usando o método reduce para somar todas as notas.
    const media = soma / notas.length; // Calculando a média dividindo a soma pelo número de notas.
    // Exibindo o resultado da média na tela com duas casas decimais usando o método toFixed().
    resultadoMedia.textContent = `Média: ${media.toFixed(2)}`;
}